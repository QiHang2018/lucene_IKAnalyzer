import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;

public class LuceneQueryTest {

    private IndexSearcher indexSearcher;
    private TopDocs result;

    @Before
    public void testInit()throws Exception{
        //创建一个indexsearcher
        DirectoryReader reader = DirectoryReader.open(FSDirectory.open(new File("/Users/huweixing/index")));
        indexSearcher = new IndexSearcher(reader);
    }

    @Test
    public void testQuery()throws Exception{
        //模糊查询
     /*   Term term = new Term("title","luceen");
        FuzzyQuery query = new FuzzyQuery(term);
        result = indexSearcher.search(query, 3);*/

         //前缀查询
        /*PrefixQuery query = new PrefixQuery(new Term("title","luce"));
        result=indexSearcher.search(query, 3);*/


        //统配查询
        /*WildcardQuery query = new WildcardQuery(new Term("title","l*"));
        result =indexSearcher.search(query, 3);*/



        //组合查询
        /*BooleanQuery booleanQuery = new BooleanQuery();
        //统配查询
        WildcardQuery wildcardQuery = new WildcardQuery(new Term("title", "l*"));
        booleanQuery.add(wildcardQuery, BooleanClause.Occur.MUST);
        //前缀查询
        PrefixQuery prefixQuery = new PrefixQuery(new Term("title", "l"));
        booleanQuery.add(prefixQuery, BooleanClause.Occur.MUST);
        result = indexSearcher.search(booleanQuery, Integer.MAX_VALUE);*/


        //意图查询
        /*Analyzer analyzer = new IKAnalyzer();
        QueryParser queryParser = new QueryParser("content", analyzer);
        Query query =queryParser.parse("学习lucene");
        result = indexSearcher.search(query, Integer.MAX_VALUE);*/

        //多字段查询
        MultiFieldQueryParser multiFieldQueryParser
                = new MultiFieldQueryParser(new String[]{"title","content"},new IKAnalyzer());
        Query query =multiFieldQueryParser.parse("学习mac");

        result = indexSearcher.search(query, Integer.MAX_VALUE);



    }

    @After
    public void testPrint()throws Exception{
//5 打印
        // 从result中获得所有被命中的文档数组
        ScoreDoc[] scoreDocs=result.scoreDocs;
        //从迭代数据中，得到ScoreDoc
        for (ScoreDoc scoreDoc:scoreDocs ) {
            //从ScoreDoc中获得一个docID
            int docID = scoreDoc.doc;
            //通过indexSeacher查询docID
            Document doc = indexSearcher.doc(docID);
            //打印返回的结果
            System.out.println(doc.get("id"));
            System.out.println(doc.get("title"));
            System.out.println(doc.get("content"));
            System.out.println(doc.get("author"));
        }
    }
}
