import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;
import java.io.IOException;

public class LuceneTest {


    //默认分词器
    @Test
    public void testCreateIndex() throws IOException {
        //准备数据
        int id=1;
        String title="lucene是构建搜索引擎的核心技术";
        String content="Lucene是一款高性能的、可扩展的信息检索（IR）工具库。信息检索是指文档搜索、文档内信息搜索或者文档相关的元数据搜索等操作";
        String author="黑马程序员";
        //创建document对象
        /**
         * StringField 对内容不进行分词
         * Textfield 对内容进行分词
         * Store.YES 是否存储原始数据（索引库中保存两种类型的数据，一种是倒排索引的数据，一种是文档的原始数据）
         */
        Document doc=new Document();
        doc.add(new IntField("id",1, Field.Store.YES));
        doc.add(new TextField("title",title, Field.Store.YES));
        doc.add(new TextField("content",content, Field.Store.YES));
        doc.add(new StringField("author",author, Field.Store.YES));

        //使用indexwriter来创建索引

        //索引存放的目录
        Directory d=FSDirectory.open(new File("/Users/huweixing/index"));

        //创建标准的分词解析器
        Analyzer analyzer=new StandardAnalyzer();
        //Version.LATEST  要对应我们的jar包的版本号
        IndexWriterConfig config=new IndexWriterConfig(Version.LATEST,analyzer);

        IndexWriter indexWriter=new IndexWriter(d,config);


        //创建索引

        indexWriter.addDocument(doc);
        indexWriter.commit();
        indexWriter.close();

    }

    //IK中文分词器
    @Test
    public void testIKAnalyzer() throws IOException {
        //准备数据
        int id=1;
        String title="lucene是构建搜索引擎的核心技术";
        String content="Lucene是一款高性能的、可扩展的信息检索（IR）工具库。信息检索是指文档搜索、文档内信息搜索或者文档相关的元数据搜索等操作";
        String author="黑马程序员";
        //创建document对象
        /**
         * StringField 对内容不进行分词
         * Textfield 对内容进行分词
         * Store.YES 是否存储原始数据（索引库中保存两种类型的数据，一种是倒排索引的数据，一种是文档的原始数据）
         */
        Document doc=new Document();
        doc.add(new IntField("id",1, Field.Store.YES));
        doc.add(new TextField("title",title, Field.Store.YES));
        doc.add(new TextField("content",content, Field.Store.YES));
        doc.add(new StringField("author",author, Field.Store.YES));

        //使用indexwriter来创建索引

        //索引存放的目录
        Directory d=FSDirectory.open(new File("/Users/huweixing/index"));

        //创建标准的分词解析器
        Analyzer analyzer=new IKAnalyzer();
        //Version.LATEST  要对应我们的jar包的版本号
        IndexWriterConfig config=new IndexWriterConfig(Version.LATEST,analyzer);

        IndexWriter indexWriter=new IndexWriter(d,config);


        //创建索引

        indexWriter.addDocument(doc);
        indexWriter.commit();
        indexWriter.close();

    }

    //词条查询
    @Test
    public void testQueryIndex() throws Exception{
        //1、指定一个需要查询的关键词
        String keyword="搜索";
        //2、将关键词转换为一个查询对象
        //查询文档中contednt字段包含关键词
        TermQuery termQuery=new TermQuery(new Term("content",keyword));
        //3、创建一个indexsearcher
        DirectoryReader reader = DirectoryReader.open(FSDirectory.open(new File("/Users/huweixing/index")));
        IndexSearcher indexSearcher=new IndexSearcher(reader);

        //4、执行查询  TopDocs并没有内容，只有文档的id,  文档id是索引库自己内部维护的
        TopDocs result = indexSearcher.search(termQuery, 100);

        //5 打印
        // 从result中获得所有被命中的文档数组
        ScoreDoc[] scoreDocs=result.scoreDocs;
        //从迭代数据中，得到ScoreDoc
        for (ScoreDoc scoreDoc:scoreDocs ) {
            //从ScoreDoc中获得一个docID
            int docID = scoreDoc.doc;
            //通过indexSeacher查询docID
            Document doc = indexSearcher.doc(docID);
            //打印返回的结果
            System.out.println(doc.get("id"));
            System.out.println(doc.get("title"));
            System.out.println(doc.get("content"));
            System.out.println(doc.get("author"));
        }


    }

    @Test//内存索引库
    public void testRam() throws Exception {
        //1）准备内存索引库
        Directory directory = new RAMDirectory();
        //2) 准备文件写入器
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LATEST, new IKAnalyzer());
        IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);
        //3) 准备文件
        Document doc = new Document();
        doc.add(new StringField("id", "1", Field.Store.YES));
        doc.add(new TextField("title", "学习lucene的秘诀", Field.Store.YES));
        doc.add(new TextField("content", "学习lucene需要掌握搜索引擎的基本原理和lucene创建索引和查询索引", Field.Store.YES));
        //4）写入文件
        indexWriter.addDocument(doc);
        indexWriter.commit();
        indexWriter.close();
    }

    @Test//把内存索引库合并到文件中
    public void testfsIndexWriter() throws Exception {
        //1）准备内存索引库
        Directory directory = new RAMDirectory();
        //2) 准备文件写入器
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LATEST, new IKAnalyzer());
        IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);
        //3) 准备文件
        Document doc = new Document();
        doc.add(new StringField("id", "1", Field.Store.YES));
        doc.add(new TextField("title", "学习lucene的秘诀", Field.Store.YES));
        doc.add(new TextField("content", "学习lucene需要掌握搜索引擎的基本原理和lucene创建索引和查询索引", Field.Store.YES));
        //4）写入文件
        indexWriter.addDocument(doc);
        indexWriter.commit();
        indexWriter.close();

        //5)准备文件索引库
        Directory fsDirectory = FSDirectory.open(new File("index"));
        IndexWriter fsIndexWriter = new IndexWriter(fsDirectory,
                new IndexWriterConfig(Version.LATEST, new IKAnalyzer()));
        //6)合并索引库
        fsIndexWriter.addIndexes(directory);
        fsIndexWriter.commit();
        fsIndexWriter.close();
    }

    @Test//高度显示
    public  void highlight() throws Exception {
        TermQuery query = new TermQuery(new Term("title", "搜索"));

        SimpleHTMLFormatter simpleHTMLFormatter = new SimpleHTMLFormatter("<font color='red'>", "</font>");
        QueryScorer queryScorer = new QueryScorer(query);
        Highlighter highlighter = new Highlighter(simpleHTMLFormatter, queryScorer);

        // 1.搜索结果
        FSDirectory d = FSDirectory.open(new File("d://indexik"));
        IndexSearcher indexSearcher = new IndexSearcher(DirectoryReader.open(d));
        TopDocs result = indexSearcher.search(query, 150);
        ScoreDoc[] scoreDocs = result.scoreDocs;
        for (ScoreDoc scoreDoc : scoreDocs) {
            Document doc = indexSearcher.doc(scoreDoc.doc);
            // 2.对结果进行再次分词，然后加上css样式
            // 高亮器的用法，先创建一个高亮器highlight
            // 使用高亮器作用于一个字段
            String title = doc.get("title");
            String bestFragment = highlighter.getBestFragment(new IKAnalyzer(), "title", title);
            System.out.println("-------------我是高亮" + bestFragment);
        }
    }

}
